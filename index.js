const express = require('express');
const bodyParser = require("body-parser");
var morgan = require('morgan');
var cors = require('cors');

const app = express();
const PORT = 3000;

// enable cors
app.use(cors());

// bodyparser setup
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json());

app.use(morgan('tiny'));

app.get('/', (req,res)=>{
    res.send('Server running on port '+PORT)
});

app.listen(PORT, ()=>{
    console.log('Server is running on port '+PORT)
});

module.exports = app;